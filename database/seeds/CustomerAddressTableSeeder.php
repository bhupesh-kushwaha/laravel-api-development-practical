<?php

use Illuminate\Database\Seeder;

class CustomerAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        \App\CustomerAddess::insert([
            'customer_id' => 1,
            'address1' => $faker->buildingNumber,
            'address2' => $faker->secondaryAddress,
            'landmark' => $faker->streetAddress,
            'pincode' => $faker->postcode,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\CustomerAddess::insert([
            'customer_id' => 2,
            'address1' => $faker->buildingNumber,
            'address2' => $faker->secondaryAddress,
            'landmark' => $faker->streetAddress,
            'pincode' => $faker->postcode,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\CustomerAddess::insert([
            'customer_id' => 3,
            'address1' => $faker->buildingNumber,
            'address2' => $faker->secondaryAddress,
            'landmark' => $faker->streetAddress,
            'pincode' => $faker->postcode,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\CustomerAddess::insert([
            'customer_id' => 4,
            'address1' => $faker->buildingNumber,
            'address2' => $faker->secondaryAddress,
            'landmark' => $faker->streetAddress,
            'pincode' => $faker->postcode,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\CustomerAddess::insert([
            'customer_id' => 5,
            'address1' => $faker->buildingNumber,
            'address2' => $faker->secondaryAddress,
            'landmark' => $faker->streetAddress,
            'pincode' => $faker->postcode,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
