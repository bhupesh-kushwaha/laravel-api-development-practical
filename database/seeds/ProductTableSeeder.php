<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        \App\Product::insert([
            'name' => $faker->catchPhrase,
            'price' => $faker->randomNumber(4),
            'special_price' => $faker->randomNumber(3),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'available_quantity' => $faker->randomNumber(2),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Product::insert([
            'name' => $faker->catchPhrase,
            'price' => $faker->randomNumber(4),
            'special_price' => $faker->randomNumber(3),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'available_quantity' => $faker->randomNumber(2),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Product::insert([
            'name' => $faker->catchPhrase,
            'price' => $faker->randomNumber(4),
            'special_price' => $faker->randomNumber(3),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'available_quantity' => $faker->randomNumber(2),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Product::insert([
            'name' => $faker->catchPhrase,
            'price' => $faker->randomNumber(4),
            'special_price' => $faker->randomNumber(3),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'available_quantity' => $faker->randomNumber(2),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Product::insert([
            'name' => $faker->catchPhrase,
            'price' => $faker->randomNumber(4),
            'special_price' => $faker->randomNumber(3),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'available_quantity' => $faker->randomNumber(2),
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
