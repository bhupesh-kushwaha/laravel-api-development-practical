<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Customer::insert([
            'username' => 'Bhupesh Kushwaha',
            'email' => 'bhupesh@demo.com',
            'password' => 'demo@123',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Customer::insert([
            'username' => 'Kushwaha Bhupesh',
            'email' => 'bhupesh2@demo.com',
            'password' => 'demo@123',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Customer::insert([
            'username' => 'Kushwaha Tester',
            'email' => 'bhupesh3@demo.com',
            'password' => 'demo@123',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Customer::insert([
            'username' => 'Bhupesh Tester',
            'email' => 'bhupesh4@demo.com',
            'password' => 'demo@123',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Customer::insert([
            'username' => 'Tester Bhupesh',
            'email' => 'bhupesh5@demo.com',
            'password' => 'demo@123',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
