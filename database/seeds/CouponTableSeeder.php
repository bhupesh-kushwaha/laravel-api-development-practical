<?php

use Illuminate\Database\Seeder;

class CouponTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        \App\Coupon::insert([
            'coupon_code' => $faker->swiftBicNumber,
            'price' => $faker->randomNumber(2),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Coupon::insert([
            'coupon_code' => $faker->swiftBicNumber,
            'price' => $faker->randomNumber(2),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Coupon::insert([
            'coupon_code' => $faker->swiftBicNumber,
            'price' => $faker->randomNumber(2),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Coupon::insert([
            'coupon_code' => $faker->swiftBicNumber,
            'price' => $faker->randomNumber(2),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\Coupon::insert([
            'coupon_code' => $faker->swiftBicNumber,
            'price' => $faker->randomNumber(2),
            'special_price_from' => '2020-01-01',
            'special_price_to' => '2020-12-31',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
