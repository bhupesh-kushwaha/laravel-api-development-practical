<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'email', 
        'password'
    ];

    /**
     * Get the address associated with the customer.
     */
    public function customerAddesses()
    {
        return $this->hasOne(CustomerAddess::class);
    }
}
