<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const SHIPPING_CHARGE = '5';

    const ORDER_PREFIX = "OD0000";

    const ORDER_STATUS_PENDING = 1;
    const ORDER_STATUS_ON_HOLD = 2;
    const ORDER_STATUS_ON_PROGRESSING = 3;
    const ORDER_STATUS_COMPLETED = 4;
    const ORDER_STATUS_CANCLED = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'customer_id', 
        'product_id', 
        'quantity', 
        'coupon_id', 
        'customer_addess_id',
        'total',
        'discount',
        'status'
    ];

    /**
     * Get the customer associated with the order.
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    /**
     * Get the product associated with the order.
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * Get the coupon associated with the order.
     */
    public function coupon()
    {
        return $this->hasOne(Coupon::class, 'id', 'coupon_id');
    }

    /**
     * Get the address associated with the order.
     */
    public function customerAddesses()
    {
        return $this->hasOne(CustomerAddess::class, 'id', 'customer_addess_id');
    }
}
