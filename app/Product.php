<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const PRODUCT_STATUS_ACTIVE = 1;
    const PRODUCT_STATUS_DEACTIVE = 0;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'price', 
        'special_price', 
        'special_price_from', 
        'special_price_to',
        'available_quantity',
        'status'
    ];
}
