<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\ApiInterfaces;
use App\Http\Traits\HelperTrait;
use App\Http\Traits\CartTrait;
use App\Http\Traits\OrderTrait;
use App\Customer;
use App\Product;
use App\Coupon;
use App\Order;
use App\Cart;

class ApiController extends Controller implements ApiInterfaces
{
    use HelperTrait, CartTrait, OrderTrait;

    /**
     * The request implementation.
     *
     * @var request
     */
    protected $request;

    /**
     * Create a new controller instance.
     *
     * @param  Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Get a customer fetch the given type from the container.
     *
     * @param  Customer  $customer
     * @return mixed
     */
    public function showcustomer(Customer $customer) 
    {   
        $custmerData = $customer->first()->load('customerAddesses');

        return $this->generateResponse(200, 'success', 'customer data',
            $this->fetchResourceData('customer', $custmerData) 
        );
    }

    /**
     * Get a product fetch the given type from the container.
     *
     * @param  Product  $product
     * @return mixed
     */
    public function showProduct(Product $product) 
    {
        $productData = $product->first();

        return $this->generateResponse(200, 'success', 'product data',
            $this->fetchResourceData('product', $productData)
        );
    }

    /**
     * Get a coupon fetch the given type from the container.
     *
     * @param string $coupon
     * @return mixed
     */
    public function showcoupon($coupon) 
    {
        $couponData = Coupon::where('coupon_code', $coupon)->first();

        if( $couponData ){
            return $this->generateResponse(200, 'success', 'coupon data',
                $this->fetchResourceData('coupon', $couponData)
            );
        }

        return $this->generateResponse(404, 'fail', 'coupon data not found');
    }
    
    /**
     * Store a cart to the given type from the container.
     *
     * @return mixed
     */
    public function addCart() 
    {
        $validator = \Validator::make($this->request->all(), [
            'customer' => 'required',
            'product' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails())
        {
            return $this->generateResponse( 422, 'fail', 'invaild data', null, 
                $validator->errors()->all()
            );
        }

        $cartData = $this->actionCart($this->request->all());

        if( isset($cartData['error_message']) ) {
            return $this->generateResponse(422, 'fail', 'invaild data', null, array( $cartData));
        }

        return $this->generateResponse(200, 'success', 'cart placed', array('cart' => $cartData), null);
    }

    /**
     * Get a cart fetch the given type from the container.
     *
     * @param string $customer
     * @param string $cart
     * @return mixed
     */
    public function showCart($customer, $cart) 
    {
        $cart = Cart::where(['customer_id' => $customer,'cart_id' => $cart])->get();

        if( $cart ) {
            $cartData = $cart->load(['product', 'coupon']);

            return $this->generateResponse(200, 'success', 'coupon data',
                $this->fetchResourceData('cart', $cartData)
            );
        }

        return $this->generateResponse(404, 'fail', 'cart data not found');
    }

     /**
     * Store a order to the given type from the container.
     *
     * @param  \Illuminate\Http\Request  $request 
     * @return mixed
     */
    public function addOrder() 
    {
        $validator = \Validator::make($this->request->all(), [
            'customer' => 'required',
            'cart' => 'required',
        ]);

        if ($validator->fails())
        {
            return $this->generateResponse(422, 'fail', 'invaild data', null, $validator->errors()->all() );
        }

        $orderData = $this->actionOrder($this->request->all());

        if( isset($orderData['error_message']) ) {
            return $this->generateResponse(422, 'fail', 'invaild data', null, array($orderData));
        }

        return $this->generateResponse(200, 'success', 'oderd placed', array('order' => $orderData),null);
    }

    /**
     * Get a order fetch the given type from the container.
     *
     * @param string $customer
     * @param string $order
     * @return mixed
     */
    public function showOrder($customer, $order) 
    {
        $order = Order::where(['customer_id' => $customer,'id' => $order])->first();

        if( $order ) {
            $orderData = $order->load(['customer', 'product', 'coupon', 'customerAddesses']);

            return $this->generateResponse(200, 'success', 'coupon data',
                $this->fetchResourceData('order', $orderData)
            );
        }

        return $this->generateResponse(404, 'fail', 'order data not found');
    }
}
