<?php

namespace App\Http\Traits;

use App\Cart;
use App\Order;

trait OrderTrait 
{
    /**
     * Generate prefix for orders.
     *
     * @return int
     */
    public function generateOrderPrefixID()
    {
        return Order::ORDER_PREFIX . (Order::latest()->latest()->first()->id + 1);
    }

    /**
     * Complate action order to store in database.
     *
     * @param object $param
     * @return array
     */
    public function actionOrder($param)
    {
        \DB::beginTransaction();

        try {
            $cartData = Cart::where('cart_id', $param['cart'])->get();

            if( $cartData->count() === 0 ) {
                throw new \Exception("Cart empty");       
            }

            $orderPrefixId = $this->generateOrderPrefixID();

            foreach( $cartData as $cart ) {
                $productStaus = $cart->product->status;
                
                if( $productStaus ) {
                    $productPrice = $this->findCartProductPrice($cart);
    
                    $dicountPrice = $this->findCartCouponPrice($cart);

                    $totalAmount = ($productPrice - $dicountPrice) + Order::SHIPPING_CHARGE;

                    $order = new Order();
                    $order->order_id = $orderPrefixId;
                    $order->customer_id = $param['customer'];
                    $order->product_id  = $cart->product_id;
                    $order->quantity  = $cart->quantity;
                    $order->coupon_id  = $cart->coupon_id;
                    $order->customer_addess_id = $cart->customer_addess_id;
                    $order->total = $totalAmount;
                    $order->discount = $dicountPrice;
                    $order->status = Order::ORDER_STATUS_PENDING;
                    $order->save;
                }
            }

            Cart::where('cart_id', $param['cart'])->delete();

            \DB::commit();

            return [
                'order_id' => $orderPrefixId,
                'order_status' => Order::ORDER_STATUS_PENDING,
            ];
        }
        catch(\Exception $e){
            \DB::rollBack();

            return [
                'error_message' => $e->getMessage(),
                'error_line' => $e->getLine(),
            ];
        }
    }

    /**
     * Find out the actual product price from cart data.
     *
     * @param object $object
     * @return float
     */
    public function findCartProductPrice($object)
    {
       $productPrice = $object->product->price;
                
       $productDataFrom = $object->product->special_price_from;
       $productDateTO = $object->product->special_price_to;

       if( $this->checkDateBetween($productDataFrom, $productDateTO) ) {
           $productPrice = $object->product->special_price;
       }

       return $productPrice;
    }

    /**
     * Find out the actual coupon price from cart data.
     *
     * @param object $object
     * @return float
     */
    public function findCartCouponPrice($object)    
    {
        $dicountPrice = 0;

        if( $object->coupon ){
            $couponDataFrom = $object->coupon->special_price_from;
            $couponDateTO = $object->coupon->special_price_to;

            if( $this->checkDateBetween($couponDataFrom, $couponDateTO) ) {
                $dicountPrice  = $object->product->special_price;
            }
        }

        return $dicountPrice;
    }

    /**
     * Check current date is between given two date or not.
     *
     * @param date $start
     * @param date $end
     * @return boolean
     */
    public function checkDateBetween($start, $end)
    {
        $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $start);

        $endDate = \Carbon\Carbon::createFromFormat('Y-m-d', $end);

        return \Carbon\Carbon::now()->between($startDate,$endDate);
    }
}