<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Response;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CouponResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\CartResource;
use App\Cart;
use App\Order;

trait HelperTrait 
{
    /**
     * Generate Json response with given parameters.
     *
     * @return mixed
     */
    public function generateResponse(
        $status_code = 200, 
        $status_message = '', 
        $message = '',  
        array $data = null,
        array $error = null
    ) {
        return Response::json([
            'status' => $status_message,
            'message' => $message,
            'data' => $data,
            'error' => $error,
        ], $status_code);
    }

    /**
     * Get any resource data using that type.
     *
     * @param string $type
     * @param object $object
     * @return array
     */
    public function fetchResourceData($type, $object) {
        switch ($type) {
            case 'customer':
                $customerResource = new CustomerResource( $object );

                $data = $this->generateArrayFromJsonResource($customerResource);

                break;

            case 'product':
                $productResource = new ProductResource( $object );

                $data = $this->generateArrayFromJsonResource($productResource);  
                
                break;
            
            case 'coupon':
                $couponResource = new CouponResource( $object );

                $data = $this->generateArrayFromJsonResource($couponResource);   
                
                break;

            case 'order':
                $orderResource = new OrderResource( $object );

                $data = $this->generateArrayFromJsonResource($orderResource);  
                
                break;    

            case 'cart':
                $cartResource = CartResource::collection( $object );

                $data = $this->generateArrayFromJsonResource($cartResource);  
                
                break;        
            
            default:
                $data = [];

                break;
        }

        return $data;
    }
    
    /**
     * Generate array from given json resource object.
     *
     * @param object $object
     * @return array
     */
    public function generateArrayFromJsonResource($object)
    {
        $array = $object->response()->getData(true);

        return isset($array['data']) ? $array['data'] : [];  
    }
}