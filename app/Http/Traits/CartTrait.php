<?php

namespace App\Http\Traits;

use App\Cart;
use App\Product;
use App\Customer;

trait CartTrait 
{
    /**
     * Complate action order to store in database.
     *
     * @param object $param
     * @return array
     */
    public function actionCart($param)
    {
        try {
            $productData = Product::where(['id' => $param['product'], 'status' => 1])->get();

            if( $productData->count() === 0 ) {
                throw new Exception("Product not active", 1);
            }

            $customer = Customer::find($param['customer'])->first();
            $customerAddressID = $customer->customerAddesses->id;

            $newUser = Cart::updateOrCreate([
                //Add unique field combo to match here
                'customer_id' => $param['customer'],
                'product_id' => $param['product']
            ],[
                'cart_id' => Cart::CART_PREFIX . $param['customer'],
                'quantity' => \DB::raw('quantity + ' . $param['quantity']),
                'coupon_id' => $param['coupon'] ?? 0,
                'customer_addess_id' => $customerAddressID
            ]);

            return [
                'cart_id' => Cart::CART_PREFIX . $param['customer'],
                'cart_status' => 'Added successfully',
            ];
        }
        catch(\Exception $e){
            \DB::rollBack();

            return [
                'error_message' => $e->getMessage(),
                'error_line' => $e->getLine(),
            ];
        }
    }
}