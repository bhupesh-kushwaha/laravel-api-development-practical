<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data['quantity'] = $this->quantity;

        if( $this->product ) {
            $data['product'] = array(
                'name' =>  $this->product->name, 
                'price' =>  $this->product->price, 
                'special_price' =>  $this->product->special_price, 
                'special_price_from' =>  $this->product->special_price_from, 
                'special_price_to' =>  $this->product->special_price_to,
                'available_quantity' =>  $this->product->available_quantity,
                'status' =>  $this->product->status
            );
        }

        if( $this->coupon ) {
            $data['coupon'] = array(
                'coupon_code' => $this->coupon->coupon_code, 
                'price' => $this->coupon->price, 
                'special_price_from' => $this->coupon->special_price_from, 
                'special_price_to' => $this->coupon->special_price_to
            );
        }

        return $data;
    }
}
