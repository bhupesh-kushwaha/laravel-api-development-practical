<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' =>  $this->name, 
            'price' =>  $this->price, 
            'special_price' =>  $this->special_price, 
            'special_price_from' =>  $this->special_price_from, 
            'special_price_to' =>  $this->special_price_to,
            'available_quantity' =>  $this->available_quantity,
            'status' =>  $this->status
        ];
    }
}
