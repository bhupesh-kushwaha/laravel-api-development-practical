<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data['customer_id'] = $this->customer_id; 
        $data['product_id'] = $this->product_id; 
        $data['quantity'] = $this->quantity; 
        $data['coupon_id'] = $this->coupon_id; 
        $data['customer_addess_id'] = $this->customer_addess_id;
        $data['total'] = $this->total;
        $data['discount'] = $this->discount;
        $data['status'] = $this->status;

        if( $this->customer ) {
            $data['customer'] = [
                'username' => $this->customer->username,
                'email' => $this->customer->email,
            ];
        }

        if( $this->customerAddesses ) {
            $data['customer_address'] = [
                'address1' => $this->customerAddesses->address1,
                'address2' => $this->customerAddesses->address2,
                'landmark' => $this->customerAddesses->landmark,
                'pincode' => $this->customerAddesses->pincode
            ];
        }

        if( $this->product ) {
            $data['product'] = [
                'name' =>  $this->product->name, 
                'price' =>  $this->product->price, 
                'special_price' =>  $this->product->special_price, 
                'special_price_from' =>  $this->product->special_price_from, 
                'special_price_to' =>  $this->product->special_price_to,
                'available_quantity' =>  $this->product->available_quantity,
                'status' =>  $this->product->status
            ];
        }

        if( $this->coupon ) {
            $data['coupon'] = [
                'coupon_code' => $this->coupon->coupon_code, 
                'price' => $this->coupon->price, 
                'special_price_from' => $this->coupon->special_price_from, 
                'special_price_to' => $this->coupon->special_price_to
            ];
        }

        return $data;
    }
}
