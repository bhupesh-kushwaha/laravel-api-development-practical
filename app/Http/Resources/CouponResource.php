<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'coupon_code' => $this->coupon_code, 
            'price' => $this->price, 
            'special_price_from' => $this->special_price_from, 
            'special_price_to' => $this->special_price_to
        ];
    }
}
