<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'username' => $this->username,
            'email' => $this->email,
            'address' => [
                'address1' => $this->customerAddesses->address1,
                'address2' => $this->customerAddesses->address2,
                'landmark' => $this->customerAddesses->landmark,
                'pincode' => $this->customerAddesses->pincode
            ]
        ];
    }
}
