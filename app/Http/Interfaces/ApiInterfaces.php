<?php 

namespace App\Http\Interfaces;

interface ApiInterfaces {

    /**
     * Get a customer fetch the given type from the container.
     *
     * @param  Customer  $customer
     * @return mixed
     */
    public function showcustomer(\App\Customer $customer);

    /**
     * Get a product fetch the given type from the container.
     *
     * @param Product $product
     * @return mixed
     */
    public function showProduct(\App\Product $product);

    /**
     * Get a coupon fetch the given type from the container.
     *
     * @param string $coupon
     * @return mixed
     */
    public function showcoupon($coupon);
    
    /**
     * Store a cart to the given type from the container.
     *
     * @return mixed
     */
    public function addCart();

    /**
     * Get a cart fetch the given type from the container.
     *
     * @param string $customer
     * @param string $cart
     * @return mixed
     */
    public function showCart($customer, $cart);

     /**
     * Store a order to the given type from the container.
     *
     * @return mixed
     */
    public function addOrder();

    /**
     * Get a order fetch the given type from the container.
     *
     * @param string $customer
     * @param string $order
     * @return mixed
     */
    public function showOrder($customer, $order);
}