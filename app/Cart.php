<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    const CART_PREFIX = "OC0000-";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cart_id',
        'customer_id', 
        'product_id', 
        'quantity', 
        'coupon_id', 
        'customer_addess_id'
    ];

    /**
     * Get the customer associated with the order.
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    /**
     * Get the product associated with the order.
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * Get the coupon associated with the order.
     */
    public function coupon()
    {
        return $this->hasOne(Coupon::class, 'id', 'coupon_id');
    }

    /**
     * Get the address associated with the order.
     */
    public function customerAddesses()
    {
        return $this->hasOne(CustomerAddess::class, 'id', 'customer_addess_id');
    }
}
