<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddess extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 
        'address1', 
        'address2',
        'landmark',
        'pincode'
    ];

    /**
     * Get the cutomer that owns the address.
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
