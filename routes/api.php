<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'V1', 'namespace' => 'Api\V1'], function () {
    Route::post( 'cart/add', 'ApiController@addCart' )->name('cart.add');
    Route::get( 'cart/show/{cutomer}/{cart}', 'ApiController@showCart' )->name('cart.show');

    Route::post( 'order/add', 'ApiController@addOrder' )->name('order.add');
    Route::get( 'order/show/{cutomer}/{order}', 'ApiController@showOrder' )->name('order.show');

    Route::get( 'product/show/{product}', 'ApiController@showProduct' )->name('product.show');

    Route::get( 'coupon/show/{coupon}', 'ApiController@showcoupon' )->name('coupon.show');

    Route::get( 'customer/show/{customer}', 'ApiController@showcustomer' )->name('customer.show');
});